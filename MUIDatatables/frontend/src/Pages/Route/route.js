import React, { Component } from 'react';
import APIServices from '../../API/apiservices';
import ChartModelDetails from '../../Component/chartModel'
import DatatableModelDetails from '../../Component/dataTableModel'
import MUIDataTableComponent from '../../Component/MuiDataTableComponent'
import color from '../../Constants/color'
import $ from 'jquery';
import '../../App.css';
import './Route.scss';

const apiServices = new APIServices();
var bcData = [];
var count = 1;
var inputLevel;
var gettingRegion;
const arrowStyle = {
  color: color.green,
  marginLeft: '5px',
  fontSize: '12px'
}

class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      routeMonthDetails: [],
      routeMonthColumns: [],
      routeHubdatas: [{
        'Hub': 'JED-International',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>12<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Hub': 'JED-Domestic',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Hub': 'RUH-International',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Hub': 'RUH-Domestic',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      ],
      routeHubColumn: ["Hub", "", 'CY', "VLY(%)", "TKT(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)"],
      routeRegiondatas: [],
      routeRegionColumn: [],
      routePOSdatas: [],
      routePOSColumn: [],
      routeRBDdatas: [],
      routeRBDColumn: [],
      routeODdatas: [],
      routeODColumn: [],
      routeLEGdatas: [],
      routeLEGColumn: [],
      routeFlightsdatas: [],
      routeFlightsColumn: [],
      modelRegionDatas: [],
      modelregioncolumn: [],
      tableDatas: true,
      gettingMonth: new Date().getMonth() + 1,
      monthTableTitle: '',
      tableTitle: '',
      tabLevel: '',
      cabinOption: [],
      cabinselectedvalue: 'Null',
      getRegionCodeKey: "Null",
      getRegionsData: "Null",
      toggle: 'lc'
    };
  }

  componentDidMount() {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = 'Null';
    var inputLevel = 'R';

    apiServices.getRouteMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var routeMonthdata = result[0].routemonthtableDatas;
      var tableTitle = result[0].monthTableTitle;
      self.setState({ routeMonthDetails: routeMonthdata, routeMonthColumns: columnName, monthTableTitle: tableTitle });
    });

    apiServices.getRouteRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var routeRegiondata = result[0].routeRegiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ routeRegiondatas: routeRegiondata, routeRegionColumn: columnName, tableTitle: tableTitle });
    });

    apiServices.getClassNameDetails().then(function (result) {
      var classData = result[0].classDatas;
      self.setState({ cabinOption: classData })
    });
  }

  monthwiseHandleClick(rowData) {
    var monththis = this;
    var { routeHubdatas, routeRegiondatas, routePOSdatas, routeRBDdatas, routeODdatas, routeLEGdatas, routeFlightsdatas } = monththis.state;
    var gettingMonth = window.monthNameToNum(rowData[0].props.children);
    var getCabinValue = this.state.cabinselectedvalue;
    var getRegionsData = this.state.getRegionsData;
    let getRegionCodeKey = '';

    // console.log("Test gettingMonth Value 1 : ", gettingMonth);
    // console.log("Test getCabinValue Value 3 : ", getCabinValue);

    if (routeHubdatas.length) {
      // console.log("Test gettingMonth Value 4 : ", gettingMonth);
      // console.log("Test getCabinValue Value 6 : ", getCabinValue);

      // apiServices.getRouteRegionTables(gettingMonth, getCabinValue).then(function (result) {
      //   inputLevel = "R";
      //   var columnName = result[0].columnName;
      //   var routeRegiondata = result[0].routeRegiontableDatas;
      //   var tableTitle = result[0].tableTitle;
      //   monththis.setState({ routeRegiondatas: routeRegiondata, routeRegionColumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      // });
    }
    else if (routeRegiondatas.length) {
      console.log("Test gettingMonth Value 4 : ", gettingMonth);
      console.log("Test getCabinValue Value 6 : ", getCabinValue);

      apiServices.getRouteRegionTables(gettingMonth, getCabinValue).then(function (result) {
        inputLevel = "R";
        var columnName = result[0].columnName;
        var routeRegiondata = result[0].routeRegiontableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ routeRegiondatas: routeRegiondata, routeRegionColumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      });

    } else if (routePOSdatas.length) {
      apiServices.getRoutePOSTables(gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routePOSdata = result[0].routePOStableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ routePOSdatas: routePOSdata, routePOSColumn: columnName, tableTitle: tableTitle });
      });

    } else if (routeRBDdatas.length) {
      apiServices.getRouteRBDTables(gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeRBDdata = result[0].routeRBDtableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({
          routeRBDdatas: routeRBDdata,
          routeRBDColumn: columnName,
          tableTitle: tableTitle,
        });
      });
    } else if (routeODdatas.length) {
      apiServices.getRouteODTables(gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeODdata = result[0].routeODtableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({
          routeODdatas: routeODdata,
          routeODColumn: columnName,
          tableTitle: tableTitle,
        });
      });
    } else if (routeLEGdatas.length) {
      apiServices.getRouteLEGTables(gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeLEGdata = result[0].routeLEGtableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({
          routeLEGdatas: routeLEGdata,
          routeLEGColumn: columnName,
          tableTitle: tableTitle,
        });
      });
    } else if (routeFlightsdatas.length) {
      apiServices.getRouteFlightsTables(gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeFlightsdata = result[0].routeFlightstableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({
          routeFlightsdatas: routeFlightsdata,
          routeFlightsColumn: columnName,
          tableTitle: tableTitle
        });
      });
    }
    bcData = [];
  }

  hubTabClick = () => {
    this.setState({
      routeHubdatas: [{
        'Hub': 'JED-International',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>12<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Hub': 'JED-Domestic',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Hub': 'RUH-International',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Hub': 'RUH-Domestic',
        '': <span className="filter_table_data" id="chartModel" data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': '1.39M',
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      ],
      routeHubColumn: ["Hub", "", 'CY', "VLY(%)", "TKT(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)", "CY(%)", "VLY(pts)"],
    })
  }

  regionTabClick = () => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    console.log("Check the Regionssss Datas getCabinValue : ", getCabinValue);

    apiServices.getRouteRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      console.log("Check the Regionssss Datas : ", result);
      var columnName = result[0].columnName;
      var routeRegiondata = result[0].routeRegiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({
        routeRegiondatas: routeRegiondata,
        routeRegionColumn: columnName,
        tableTitle: tableTitle,
        routeHubdatas: [],
        routePOSdata: [],
        routeRBDdata: [],
        routeODdatas: [],
        routeLEGdatas: [],
        routeFlightsdatas: []
      });
    });
  }

  posTabClick = () => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var getRegionsData = this.state.getRegionsData;
    var getRegionCodeKey = this.state.getRegionCodeKey;
    if (this.state.getRegionCodeKey === "Null") {
      getRegionCodeKey = "P";
    }
    console.log("Check the details POS getRegionCodeKey : ", getRegionCodeKey);
    console.log("Check the details POS getRegionsData : ", getRegionsData);

    apiServices.getRoutePOSTables(this.state.gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var routePOSdata = result[0].routePOStableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({
        routePOSdatas: routePOSdata,
        routePOSColumn: columnName,
        tableTitle: tableTitle,
        routeHubdatas: [],
        routeRegiondatas: [],
        routeRBDdata: [],
        routeODdatas: [],
        routeLEGdatas: [],
        routeFlightsdatas: []
      });
    });
    bcData = [];
  }

  rbdTabClick = () => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var getRegionsData = this.state.getRegionsData;
    var getRegionCodeKey = this.state.getRegionCodeKey;
    if (this.state.getRegionCodeKey === "Null") {
      getRegionCodeKey = "S";
    }

    console.log("Check the details RBD getRegionCodeKey : ", getRegionCodeKey);
    console.log("Check the details RBD getRegionsData : ", getRegionsData);

    apiServices.getRouteRBDTables(this.state.gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var routeRBDdata = result[0].routeRBDtableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({
        routeRBDdatas: routeRBDdata,
        routeRBDColumn: columnName,
        tableTitle: tableTitle,
        routeHubdatas: [],
        routeRegiondatas: [],
        routePOSdatas: [],
        routeODdatas: [],
        routeLEGdatas: [],
        routeFlightsdatas: []
      });
    });
    bcData = [];
  }

  odTabClick = () => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var getRegionsData = this.state.getRegionsData;
    var getRegionCodeKey = this.state.getRegionCodeKey;
    if (this.state.getRegionCodeKey === "Null") {
      getRegionCodeKey = "O";
    }

    console.log("Check the details OD getRegionCodeKey : ", getRegionCodeKey);
    console.log("Check the details OD getRegionsData : ", getRegionsData);

    apiServices.getRouteODTables(this.state.gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var routeODdata = result[0].routeODtableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({
        routeODdatas: routeODdata,
        routeODColumn: columnName,
        tableTitle: tableTitle,
        routeHubdatas: [],
        routeRegiondatas: [],
        routePOSdatas: [],
        routeRBDdata: [],
        routeLEGdatas: [],
        routeFlightsdatas: []
      });
    });
    bcData = [];
  }

  legTabClick = () => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var getRegionsData = this.state.getRegionsData;
    var getRegionCodeKey = this.state.getRegionCodeKey;
    if (this.state.getRegionCodeKey === "Null") {
      getRegionCodeKey = "L";
    }

    console.log("Check the details LEG getRegionCodeKey : ", getRegionCodeKey);
    console.log("Check the details LEG getRegionsData : ", getRegionsData);

    apiServices.getRouteLEGTables(this.state.gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var routeLEGdata = result[0].routeLEGtableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({
        routeLEGdatas: routeLEGdata,
        routeLEGColumn: columnName,
        tableTitle: tableTitle,
        routeHubdatas: [],
        routeRegiondatas: [],
        routePOSdatas: [],
        routeRBDdata: [],
        routeODdatas: [],
        routeFlightsdatas: []
      });
    });
    bcData = [];
  }

  flightsTabClick = () => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var getRegionsData = this.state.getRegionsData;
    var getRegionCodeKey = this.state.getRegionCodeKey;
    if (this.state.getRegionCodeKey === "Null") {
      getRegionCodeKey = "F";
    }

    console.log("Check the details Flights getRegionCodeKey : ", getRegionCodeKey);
    console.log("Check the details Flights getRegionsData : ", getRegionsData);

    apiServices.getRouteFlightsTables(this.state.gettingMonth, getRegionCodeKey, getRegionsData, getCabinValue).then(function (result) {
      console.log("Rsult Dats ; ", result);
      var columnName = result[0].columnName;
      var routeFlightsdata = result[0].routeFlightstableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({
        routeFlightsdatas: routeFlightsdata,
        routeFlightsColumn: columnName,
        tableTitle: tableTitle,
        routeHubdatas: [],
        routeRegiondatas: [],
        routePOSdatas: [],
        routeRBDdata: [],
        routeODdatas: [],
        routeLEGdatas: [],
      });
    });
    bcData = [];
  }

  cabinSelectChange = (e) => {
    var self = this;
    var { routeHubdatas, routeRegiondatas, routePOSdatas, routeRBDdatas, routeODdatas, routeLEGdatas, routeFlightsdatas } = self.state;
    var getCabinValue = e.target.value;
    var gettingMonth = this.state.gettingMonth;
    var getColName = $('.hubTable thead tr th:nth-child(1)').text();
    var gettingRegion = 'Null';
    var getRegionCodeKey='Null';
    var inputLevel = 'R';

    self.setState({ cabinselectedvalue: e.target.value });

    console.log("Check the Find 1 : ", getCabinValue);
    console.log("Check the Find 2 : ", e.target.value);

    apiServices.getRouteMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      console.log("Check The returnsss datas 1 : ", result);
      var columnName = result[0].columnName;
      var routemonthtableData = result[0].routemonthtableDatas;
      var tableTitle = result[0].monthTableTitle;
      self.setState({ routeMonthDetails: routemonthtableData, monthcolumns: columnName, monthTableTitle: tableTitle })
    });

    if (routeRegiondatas.length) {
      console.log("Test gettingMonth Value 4 : ", gettingMonth);
      console.log("Test getCabinValue Value 6 : ", getCabinValue);

      apiServices.getRouteRegionTables(gettingMonth, getCabinValue).then(function (result) {
        inputLevel = "R";
        var columnName = result[0].columnName;
        var routeRegiondata = result[0].routeRegiontableDatas;
        var tableTitle = result[0].tableTitle;
        self.setState({ routeRegiondatas: routeRegiondata, routeRegionColumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      });

    } else if (routePOSdatas.length) {
      apiServices.getRoutePOSTables(gettingMonth, getRegionCodeKey, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routePOSdata = result[0].routePOStableDatas;
        var tableTitle = result[0].tableTitle;
        self.setState({ routePOSdatas: routePOSdata, routePOSColumn: columnName, tableTitle: tableTitle });
      });

    } else if (routeRBDdatas.length) {
      apiServices.getRouteRBDTables(gettingMonth, getRegionCodeKey, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeRBDdata = result[0].routeRBDtableDatas;
        var tableTitle = result[0].tableTitle;
        self.setState({
          routeRBDdatas: routeRBDdata,
          routeRBDColumn: columnName,
          tableTitle: tableTitle,
        });
      });
    } else if (routeODdatas.length) {
      apiServices.getRouteODTables(gettingMonth, getRegionCodeKey, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeODdata = result[0].routeODtableDatas;
        var tableTitle = result[0].tableTitle;
        self.setState({
          routeODdatas: routeODdata,
          routeODColumn: columnName,
          tableTitle: tableTitle,
        });
      });
    } else if (routeLEGdatas.length) {
      apiServices.getRouteLEGTables(gettingMonth, getRegionCodeKey, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeLEGdata = result[0].routeLEGtableDatas;
        var tableTitle = result[0].tableTitle;
        self.setState({
          routeLEGdatas: routeLEGdata,
          routeLEGColumn: columnName,
          tableTitle: tableTitle,
        });
      });
    } else if (routeFlightsdatas.length) {
      apiServices.getRouteFlightsTables(gettingMonth, getRegionCodeKey, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeFlightsdata = result[0].routeFlightstableDatas;
        var tableTitle = result[0].tableTitle;
        self.setState({
          routeFlightsdatas: routeFlightsdata,
          routeFlightsColumn: columnName,
          tableTitle: tableTitle
        });
      });
    }

  }

  RegionhandleClick(rowData) {
    var regionthis = this;
    var gettingRegion = rowData[0].props.children;
    var gettingMonth = this.state.gettingMonth;
    var getColName = $('.regionTable thead tr th:nth-child(1)').text();
    var getCabinValue = this.state.cabinselectedvalue;
    var inputLevel;
    console.log("Check The gettingRegion : ", gettingRegion);
    console.log("Check The gettingMonth : ", gettingMonth);
    console.log("Check The getColName : ", getColName);
    console.log("Check The getCabinValue : ", getCabinValue);

    if (getColName === "Region" || getColName === "RegionName" || getColName === "CountryName") {
      console.log("Check The If Condiditonsss : ", getColName);
      regionthis.setState({ routeMonthDetails: [], routeRegiondatas: [] });
    } else {
      regionthis.setState({ getRegionCodeKey: "RC", getRegionsData: gettingRegion });
    }

    if (getColName === "Region") {
      inputLevel = "R";
      apiServices.getRouteRegionDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeRegionstableData = result[0].routeRegionstableDatas;
        var tableTitle = result[0].tableTitle;
        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Region" });
        regionthis.setState({ routeRegiondatas: routeRegionstableData, routeRegionColumn: columnName, tableTitle: tableTitle, tabLevel: inputLevel, getRegionCodeKey: "R", getRegionsData: gettingRegion });
      });
      apiServices.getRouteMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeMonthdata = result[0].routemonthtableDatas;
        var tableTitle = result[0].monthTableTitle;
        regionthis.setState({ routeMonthDetails: routeMonthdata, routeMonthColumns: columnName, monthTableTitle: tableTitle });
      });
    } else if (getColName === "RegionName") {
      inputLevel = "C";
      apiServices.getRouteCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeRegionstableData = result[0].routeCountrystableDatas;
        var tableTitle = result[0].tableTitle;
        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Region" });
        regionthis.setState({ routeRegiondatas: routeRegionstableData, routeRegionColumn: columnName, tableTitle: tableTitle, tabLevel: inputLevel, getRegionCodeKey: "R", getRegionsData: gettingRegion });
      });
      apiServices.getRouteMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeMonthdata = result[0].routemonthtableDatas;
        var tableTitle = result[0].monthTableTitle;
        regionthis.setState({ routeMonthDetails: routeMonthdata, routeMonthColumns: columnName, monthTableTitle: tableTitle });
      });

    } else if (getColName === "CountryName") {
      inputLevel = "RC";
      apiServices.getRouteRCDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var routeRCtableData = result[0].routeRCtableDatas;
        var tableTitle = result[0].tableTitle;
        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Country" });
        regionthis.setState({ routeRegiondatas: routeRCtableData, routeRegionColumn: columnName, tableTitle: tableTitle, tabLevel: inputLevel, getRegionCodeKey: "C", getRegionsData: gettingRegion });
      });
      apiServices.getRouteMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
        console.log("CHeck the datas : ", result[0].routemonthtableDatas);
        var columnName = result[0].columnName;
        var routeMonthdata = result[0].routemonthtableDatas;
        var tableTitle = result[0].monthTableTitle;
        regionthis.setState({ routeMonthDetails: routeMonthdata, routeMonthColumns: columnName, monthTableTitle: tableTitle });
      });

    }
    count = 2;
  }

  posHandleClick = (e) => {
    var region = this;
    apiServices.getPOSRegionTables(this.state.gettingMonth).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      region.setState({ posRegionDatas: posRegionata, routeRegionColumn: columnName, tableTitle: tableTitle })
    });
  }

  listHandleClick = (e) => {
    var regionthis = this;
    gettingRegion = e.target.id;
    var gettingMonth = this.state.gettingMonth;
    var getColName = e.target.title;

    // var inputLevel;

    var indexEnd = bcData.findIndex(function (e) {
      return e.val == gettingRegion;
    })
    var removeArrayIndex = bcData.slice(0, indexEnd + 1);
    bcData = removeArrayIndex;

    if (getColName === 'Region') {
      inputLevel = "C";
      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, routeRegionColumn: columnName, tableTitle: tableTitle })
      });
      apiServices.getMonthWise(gettingRegion, inputLevel).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posmonthtableDatas;
        var tableTitle = result[0].monthTableTitle;
        regionthis.setState({ posMonthDetails: posRegionata, routeMonthColumns: columnName, monthTableTitle: tableTitle })
      });
    } else if (getColName === 'Country') {
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, routeRegionColumn: columnName, tableTitle: tableTitle })
      });
      apiServices.getMonthWise(gettingRegion, inputLevel).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posmonthtableDatas;
        var tableTitle = result[0].monthTableTitle;
        regionthis.setState({ posMonthDetails: posRegionata, routeMonthColumns: columnName, monthTableTitle: tableTitle })
      });
    } else if (getColName === 'POS') {
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, routeRegionColumn: columnName, tableTitle: tableTitle })
      });
      apiServices.getMonthWise(gettingRegion, inputLevel).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posmonthtableDatas;
        var tableTitle = result[0].monthTableTitle;
        regionthis.setState({ posMonthDetails: posRegionata, routeMonthColumns: columnName, monthTableTitle: tableTitle })
      });
    }

  }

  cellhandleClick = (cellIndex, rowData) => {
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var self = this;
      apiServices.getPOSRegionTables(this.state.gettingMonth).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    }
  }

  toggle(val) {
    this.setState({ toggle: val })
  }

  render() {
    const options = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 6,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: (rowData, rowIndex) => {
        console.log("Check Row Data : ", rowData);
        var rowindex = (rowIndex.rowIndex) + 1;
        $(".monthTable tbody tr").css("background-color", "#fff");
        $(".monthTable tbody tr td").css("color", "#000");

        $(".monthTable tbody tr:nth-child(" + rowindex + ")").css("background-color", "#537790c2");
        $(".monthTable tbody tr#MUIDataTableBodyRow-" + rowIndex.dataIndex + " > td").css("color", "#fff")
        this.monthwiseHandleClick(rowData);
      },
      onCellClick: (cellIndex, rowData) => {
        // this.cellhandleClick(cellIndex,rowData)
      },
    };

    const regionOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 6,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: rowData => {
        this.RegionhandleClick(rowData)
      },
      // onCellClick: (cellIndex,rowData) => {
      //   this.cellhandleClick(cellIndex,rowData)
      // },
    };

    $("#pagination-next").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });
    $("#pagination-back").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });

    return (
      <div className='route'>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12 top">
            <div className="navdesign">
              <div className="col-md-5 col-sm-5 col-xs-12 toggle1">
                <section>
                  <button className={`btn`} onClick={() => this.props.history.push('/pospage')}>POS</button>
                  <button className='btn active-btn' onClick={() => this.props.history.push('/routepage')}>Route</button>
                </section>
              </div>

              <div className="col-md-5 col-sm-5 col-xs-12 toggle2">
                <button className={`btn ${this.state.toggle === 'lc' ? 'active-btn' : ''}`} onClick={() => this.toggle('lc')}>LC($)</button>
                <button className={`btn ${this.state.toggle === 'bc' ? 'active-btn' : ''}`} onClick={() => this.toggle('bc')}>BC(SR)</button>
              </div>

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12" style={{ backgroundColor: color.gray, marginTop: '15px' }}>
            <div className="navdesign">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <section>
                  <nav>
                    <ol className="cd-breadcrumb">
                      <li onClick={this.homeHandleClick} > NETWORK </li>
                      {bcData.map((item, i) =>
                        <li key={i} onClick={this.listHandleClick} data-value={item.key} id={item.val} title={item.title}> {item.val} </li>
                      )}
                    </ol>
                  </nav>
                </section>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="form-group" style={{ marginTop: "14px", marginBottom: "14px" }}>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Cabin</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

            </div>

          </div>
        </div>


        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">

            <div className="x_panel" style={{ marginTop: "15px" }}>
              <div className="x_content">

                <MUIDataTableComponent
                  datatableClassName={'monthTable'}
                  data={this.state.routeMonthDetails}
                  columns={this.state.routeMonthColumns}
                  options={options}
                  title={''}
                />

              </div>
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="x_panel">
              <div className="x_content">

                <div className="tab" id="posTableTab" role="tabpanel">
                  <ul className="nav nav-tabs" role="tablist">
                    <li role="presentation" className="active" onClick={this.hubTabClick}><a href="#Section1" aria-controls="profile" role="tab" data-toggle="tab">Hub</a></li>
                    <li role="presentation" onClick={this.regionTabClick}><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Region</a></li>
                    <li role="presentation" onClick={this.posTabClick}><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">POS Based</a></li>
                    <li role="presentation" onClick={this.rbdTabClick}><a href="#Section4" aria-controls="messages" role="tab" data-toggle="tab">RBD Based</a></li>
                    <li role="presentation" onClick={this.odTabClick}><a href="#Section5" aria-controls="messages" role="tab" data-toggle="tab">O&D Based</a></li>
                    <li role="presentation" onClick={this.legTabClick}><a href="#Section6" aria-controls="messages" role="tab" data-toggle="tab">Leg Based</a></li>
                    <li role="presentation" onClick={this.flightsTabClick}><a href="#Section7" aria-controls="messages" role="tab" data-toggle="tab">Flights Based</a></li>
                  </ul>
                  <div className="tab-content tabs">
                    <div role="tabpanel" className="tab-pane fade in active" id="Section1">

                      <MUIDataTableComponent
                        datatableClassName={'hubTable'}
                        data={this.state.routeHubdatas}
                        columns={this.state.routeHubColumn}
                        options={regionOptions}
                        title={'Hub wise Performance'}
                      />

                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="Section2">

                      <MUIDataTableComponent
                        datatableClassName={'regionTable'}
                        data={this.state.routeRegiondatas}
                        columns={this.state.routeRegionColumn}
                        options={regionOptions}
                        title={this.state.tableTitle}
                      />

                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="Section3">

                      <MUIDataTableComponent
                        datatableClassName={'posTable'}
                        data={this.state.routePOSdatas}
                        columns={this.state.routePOSColumn}
                        options={regionOptions}
                        title={this.state.tableTitle}
                      />

                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="Section4">

                      <MUIDataTableComponent
                        datatableClassName={'rbdTable'}
                        data={this.state.routeRBDdatas}
                        columns={this.state.routeRBDColumn}
                        options={regionOptions}
                        title={this.state.tableTitle}
                      />

                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="Section5">

                      <MUIDataTableComponent
                        datatableClassName={'odTable'}
                        data={this.state.routeODdatas}
                        columns={this.state.routeODColumn}
                        options={regionOptions}
                        title={this.state.tableTitle}
                      />

                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="Section6">

                      <MUIDataTableComponent
                        datatableClassName={'legTable'}
                        data={this.state.routeLEGdatas}
                        columns={this.state.routeLEGColumn}
                        options={regionOptions}
                        title={this.state.tableTitle}
                      />

                    </div>
                    <div role="tabpanel" className="tab-pane fade" id="Section7">

                      <MUIDataTableComponent
                        datatableClassName={'flightTable'}
                        data={this.state.routeFlightsdatas}
                        columns={this.state.routeFlightsColumn}
                        options={regionOptions}
                        title={this.state.tableTitle}
                      />

                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div>
          <ChartModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>

      </div>

    );
  }
}

export default Routes
