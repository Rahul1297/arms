import React, { Component } from 'react';
import APIServices from '../../API/apiservices';
import { images } from '../../Constants/images';
import ChartModelDetails from '../../Component/chartModel';
import DatatableModelDetails from '../../Component/dataTableModel';
import MUIDataTableComponent from '../../Component/MuiDataTableComponent';
import DataTableComponent from '../../Component/DataTableComponent';
import color from '../../Constants/color'
import $ from 'jquery';
import '../../App';
import './TopMarkets.scss';

const apiServices = new APIServices();

var bcData = [];
var count = 1;
var inputLevel;
var gettingRegion;
var datatableClassName;
const arrowStyle = {
  color: color.green,
  marginLeft: '5px',
  fontSize: '12px'
}

const width = window.innerWidth;
const column_width= width < 720 ?  width/5 : width/18

class TopMarkets extends Component {


  constructor(props) {
    super(props);
    this.state = {
      top5ODColumn: [
        {
          headerName: '',
          children: [{
            headerName: 'OD', field: 'OD', width: 140,
          }],
          cellStyle: { color: 'red' }
        },
        {
          headerName: 'MIDT Booked Passenger',
          children: [{ headerName: 'CY', field: 'CY_MIDT', width: column_width }, { headerName: 'LY', field: 'LY_MIDT', width: column_width }, { headerName: 'VLY%', field: 'VLY%_MIDT', cellRenderer: (params) => this.deltaIndicator(params), width: column_width }, { headerName: 'YTD', field: 'YTD_MIDT', width: column_width }]
        },
        {
          headerName: 'MIDT Airlines Booked Passenger',
          children: [{ headerName: 'CY', field: 'CY_MIDTA', width: column_width }, { headerName: 'LY', field: 'LY_MIDTA', width: column_width }, { headerName: 'VLY%', field: 'VLY%_MIDTA', cellRenderer: (params) => this.deltaIndicator(params), width: column_width }]
        },
        {
          headerName: 'O & D Passenger Forecast',
          children: [{ headerName: 'Forecast', field: 'Forecast_OD', width: column_width }, { headerName: 'TGT', field: 'TGT_OD', width: column_width }, { headerName: 'VTG(%)', field: 'VTG(%)_OD',cellRenderer: (params) => this.deltaIndicator(params), width: column_width }, { headerName: 'VLY(%)', field: 'VLY(%)_OD',cellRenderer: (params) => this.deltaIndicator(params), width: column_width }]
        },
        {
          headerName: 'Revenue',
          children: [{ headerName: 'VTG(%)', field: 'VTG(%)_REV',cellRenderer: (params) => this.deltaIndicator(params), width: column_width }]
        },
        {
          headerName: 'AL Market Share(%)',
          children: [{ headerName: 'CY', field: 'CY_AL', width: column_width }, { headerName: 'VLY(%)', field: 'VLY(%)_AL',cellRenderer: (params) => this.deltaIndicator(params), width: column_width }, { headerName: 'YTD', field: 'YTD_AL', width: column_width }]
        },
        {
          headerName: 'AL Market Share(%)',
          children: [{ headerName: 'TOP Competitor %', field: 'TOP Competitor %_ALM', width: 170 }]
        }
      ],
      top5ODData: [
        {
          'OD': 'CAI-RUH',
          'CY_MIDT': '45.98',
          'LY_MIDT': '45.67',
          'VLY%_MIDT': '10',
          'YTD_MIDT': '0.10M',
          'CY_MIDTA': '3',
          'LY_MIDTA': '87',
          'VLY%_MIDTA': '5',
          'Forecast_OD': '20',
          'TGT_OD': '18',
          'VTG(%)_OD': '10',
          'VLY(%)_OD': '8',
          'VTG(%)_REV': '8',
          'CY_AL': '10',
          'VLY(%)_AL': '5',
          'YTD_AL': '9',
          'TOP Competitor %_ALM': 'SV(50)',
        },
        {
          'OD': 'DXI-LHR',
          'CY_MIDT': '45.98',
          'LY_MIDT': '45.67',
          'VLY%_MIDT': '5',
          'YTD_MIDT': '0.10M',
          'CY_MIDTA': '3',
          'LY_MIDTA': '87',
          'VLY%_MIDTA': '11',
          'Forecast_OD': '20',
          'TGT_OD': '11',
          'VTG(%)_OD': '6',
          'VLY(%)_OD': '10',
          'VTG(%)_REV': '9',
          'CY_AL': '10',
          'VLY(%)_AL': '5',
          'YTD_AL': '9',
          'TOP Competitor %_ALM': 'SV(50)',
        },
        {
          'OD': 'FRA-ICN',
          'CY_MIDT': '45.98',
          'LY_MIDT': '45.67',
          'VLY%_MIDT': '7',
          'YTD_MIDT': '0.10M',
          'CY_MIDTA': '3',
          'LY_MIDTA': '87',
          'VLY%_MIDTA': '11',
          'Forecast_OD': '20',
          'TGT_OD': '11',
          'VTG(%)_OD': '16',
          'VLY(%)_OD': '8',
          'VTG(%)_REV': '16',
          'CY_AL': '10',
          'VLY(%)_AL': '10',
          'YTD_AL': '9',
          'TOP Competitor %_ALM': 'SV(50)',
        },
        {
          'OD': 'HKG-LHR',
          'CY_MIDT': '45.98',
          'LY_MIDT': '45.67',
          'VLY%_MIDT': '10',
          'YTD_MIDT': '0.10M',
          'CY_MIDTA': '3',
          'LY_MIDTA': '87',
          'VLY%_MIDTA': '11',
          'Forecast_OD': '20',
          'TGT_OD': '11',
          'VTG(%)_OD': '4',
          'VLY(%)_OD': '10',
          'VTG(%)_REV': '16',
          'CY_AL': '10',
          'VLY(%)_AL': '17',
          'YTD_AL': '9',
          'TOP Competitor %_ALM': 'SV(50)',
        },
        {
          'OD': 'CAI-RUH',
          'CY_MIDT': '45.98',
          'LY_MIDT': '45.67',
          'VLY%_MIDT': '10',
          'YTD_MIDT': '0.10M',
          'CY_MIDTA': '3',
          'LY_MIDTA': '87',
          'VLY%_MIDTA': '11',
          'Forecast_OD': '20',
          'TGT_OD': '11',
          'VTG(%)_OD': '6',
          'VLY(%)_OD': '6',
          'VTG(%)_REV': '18',
          'CY_AL': '10',
          'VLY(%)_AL': '5',
          'YTD_AL': '9',
          'TOP Competitor %_ALM': 'SV(50)',
        }
      ],
      top5Column: ['Airline', 'CY', 'VLY(%)', 'YTD', 'CY', 'VLY(%)', 'YTD'],
      top10Column: ['Name', 'CY', 'VLY(%)', 'YTD', 'CY', 'VLY(%)', 'YTD', 'CY(ABS)'],
      top5Data: [
        {
          'Airline': 'CX1',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
        },
        {
          'Airline': 'CX2',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
        },
        {
          'Airline': 'CX3',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
        }
      ],
      top10Data: [
        {
          'Name': 'OMEGA_TRAVELS_LTD1',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY(ABS)': '10'
        },
        {
          'Name': 'OMEGA_TRAVELS_LTD2',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY(ABS)': '10'
        },
        {
          'Name': 'OMEGA_TRAVELS_LTD3',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY(ABS)': '10'
        }
      ],
      odName: '',
      competitorName: '',
      modelRegionDatas: [],
      modelregioncolumn: [],
      tableDatas: true,
      gettingMonth: new Date().getMonth() + 1,
      tableTitle1: '',
      tableTitle2: '',
      // monthTableTitle:'',
      top5ODTitle: 'TOP 5 ODs',
      tabLevel: 'Null',
      posFlowData: 'Null',
      posAgentFlowDatas: [],
      posAgentFlowcolumn: [],
      cabinOption: [],
      cabinselectedvalue: 'Null',
    };

  }

  componentDidMount() {
    var odName = this.state.top5ODData[0].OD;
    var competitorName = this.state.top5Data[0].Airline
    this.setState({
      tableTitle1: `Top 5 Competitors for ${odName}`,
      tableTitle2: `Top 10 Agents for ${odName} & ${competitorName}`,
      odName: odName
    })
    // var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // apiServices.getClassNameDetails().then(function (result) {
    //   var classData = result[0].classDatas;
    //   self.setState({ cabinOption: classData })
    // });

  }

  // handleClick(rowData) {
  //   var monththis = this;
  //   var gettingMonth = window.monthNameToNum(rowData[0].props.children);
  //   var getColName = $('.regionTable thead tr th:nth-child(1)').text();
  //   var getCabinValue = this.state.cabinselectedvalue;

  //   if (getColName === 'RegionName') {
  //     console.log("1 is If", getColName);
  //     apiServices.getPOSRegionTables(gettingMonth, getCabinValue).then(function (result) {
  //       inputLevel = "R";
  //       var columnName = result[0].columnName;
  //       var posRegionata = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   } else if (getColName === 'CountryName') {
  //     console.log("2 is If")
  //     inputLevel = "C";
  //     apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posCountrytableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });
  //   } else if (getColName === 'POS') {
  //     console.log("3 is If")
  //     inputLevel = "P";
  //     apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   } else if (getColName === 'CommonOD') {
  //     console.log("4 is If")
  //     inputLevel = "O";
  //     apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   }
  //   else if (getColName === 'OD') {
  //     console.log("5 is If")
  //     apiServices.getODFlowTables(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posODdata = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
  //     });

  //   }
  //   else if (getColName === 'Agent') {
  //     console.log("6 is If")
  //     apiServices.getAgentDetails(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posAgentData = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
  //     });

  //   }
  //   // gettingMonth, gettingRegion, gettingRegionCode, getCabinValue
  //   //   apiServices.getAgentDetails(gettingMonth,inputLevel,posFlowData,getCabinValue).then(function (result) {
  //   //     var columnName = result[0].columnName;
  //   //     var posAgentData = result[0].posregiontableDatas;
  //   //     var tableTitle = result[0].tableTitle;
  //   //     region.setState({ posRegionDatas: posAgentData, regioncolumn:columnName, tableTitle: tableTitle})
  //   // });
  //   bcData = [];
  // }

  odhandleClick(rowData) {

    var gettingOD = rowData[0];
    var gettingCompetitor = this.state.top5Data[0].Airline
    this.setState({
      tableTitle1: `Top 5 Competitors for ${gettingOD}`,
      tableTitle2: `Top 10 Agents for ${gettingOD} & ${gettingCompetitor}`,
      odName: gettingOD
    })

    // var gettingMonth = this.state.gettingMonth;
    // var getColName = $('.regionTable thead tr th:nth-child(1)').text();
    // var getCabinValue = this.state.cabinselectedvalue;

    // if (getColName === 'RegionName') {
    //   inputLevel = "C";
    //   apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;

    //     bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Region" });

    //     regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'R', posFlowData: gettingRegion })
    //   });

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });
    // }
  }

  competitorAgentHandleClick = (e) => {

    this.setState({ tableTitle1: `Top 5 Competitors for ${this.state.odName}`, tableTitle2: `Top 10 Agents for ${this.state.odName} & ${this.state.competitorName}` })

    // var region = this;
    // region.setState({ posRegionDatas: [] })

    // var inputLevel = this.state.tabLevel;
    // var posFlowData = this.state.posFlowData;
    // var getCabinValue = this.state.cabinselectedvalue;
    // var tabValueChange;

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // if (inputLevel === 'R' && posFlowData === 'Null') {
    //   apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posRegionata = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else if (inputLevel === 'R' && posFlowData != 'Null') {
    //   tabValueChange = 'C';
    //   apiServices.getPOSCountryDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });

    // } else if (inputLevel === 'C') {
    //   tabValueChange = 'P';
    //   apiServices.getPOSDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else {
    //   tabValueChange = 'O';
    //   apiServices.getPOSODDetails(this.state.gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     console.log('pos od details', result)
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // }
  }

  handleCompetitorsClick = (rowData) => {
    var competitorName = rowData[0];
    this.setState({
      competitorName: competitorName,
      tableTitle2: `Top 10 Agents for ${this.state.odName} & ${competitorName}`,
    })
  }

  handleCompetitorsCellClick = (cellData, cellMeta) => {
    let colIndex = cellMeta.colIndex
    if (colIndex === 0) {
      window.localStorage.setItem('Competitor', cellData)
      this.props.history.push('/competitorAnalysis')
    }
  }

  handleAgentClick = (rowData) => {
    window.localStorage.setItem('Agent', rowData[0])
    this.props.history.push('/agentAnalysis')
  }

  // odHandleClick = (e) => {
  //   var region = this;
  //   region.setState({ posRegionDatas: [] })

  //   var getCabinValue = this.state.cabinselectedvalue;
  //   var tabValueChange = this.state.tabLevel;
  //   var tabValueChange;
  //   if (this.state.tabLevel === 'R' && this.state.posFlowData === 'Null') {
  //     tabValueChange = 'R';
  //   } else if (this.state.tabLevel === 'R' && this.state.posFlowData != 'Null') {
  //     tabValueChange = 'R';
  //   } else if (this.state.tabLevel === 'C') {
  //     tabValueChange = 'C';
  //   } else if (this.state.tabLevel === 'P') {
  //     tabValueChange = 'P';
  //   }
  //   apiServices.getODFlowTables(this.state.gettingMonth, tabValueChange, this.state.posFlowData, getCabinValue).then(function (result) {
  //     var columnName = result[0].columnName;
  //     var posODdata = result[0].posregiontableDatas;
  //     var tableTitle = result[0].tableTitle;
  //     region.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
  //   });
  // }

  // agentHandleClick = (e) => {
  //   var region = this;
  //   region.setState({ posRegionDatas: [] })

  //   var getCabinValue = this.state.cabinselectedvalue;
  //   var gettingMonth = this.state.gettingMonth;
  //   var inputLevel = this.state.tabLevel;
  //   var posFlowData = this.state.posFlowData;

  //   apiServices.getAgentDetails(gettingMonth, inputLevel, posFlowData, getCabinValue).then(function (result) {
  //     var columnName = result[0].columnName;
  //     var posAgentData = result[0].posregiontableDatas;
  //     var tableTitle = result[0].tableTitle;
  //     region.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
  //   });
  // }

  homeHandleClick = (e) => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";
    // apiServices.getPOSMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });
    bcData = [];
  }

  listHandleClick = (e) => {
    var regionthis = this;
    gettingRegion = e.target.id;
    var gettingMonth = this.state.gettingMonth;
    var getColName = e.target.title;
    var getCabinValue = this.state.cabinselectedvalue;

    var indexEnd = bcData.findIndex(function (e) {
      return e.val == gettingRegion;
    })
    var removeArrayIndex = bcData.slice(0, indexEnd + 1);
    bcData = removeArrayIndex;

    if (getColName === 'Region') {
      inputLevel = "C";
      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'Country') {
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'POS') {
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    }

  }

  cellhandleClick = (cellIndex, rowData) => {
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var self = this;
      var getCabinValue = this.state.cabinselectedvalue;
      apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    }
  }

  cabinSelectChange = (e) => {
    var self = this;
    bcData = [];
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = 'Null';
    var inputLevel = 'R';

    self.setState({ cabinselectedvalue: e.target.value });

    // apiServices.getMonthWise(gettingRegion, inputLevel, e.target.value).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, e.target.value).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

  }

  deltaIndicator(params) {
    var element = document.createElement("span");
    var icon = document.createElement("i");

    // visually indicate if this months value is higher or lower than last months value
    if (params.value > 9) {
      icon.className = 'fa fa-arrow-up'
    } else {
      icon.className = 'fa fa-arrow-down'
    }
    element.appendChild(document.createTextNode(params.value));
    element.appendChild(icon);

    return element;
  }

  // onSelectionChanged() {
  //   var selectedRows = api.getSelectedRows();
  //   console.log('rows', selectedRows)
  // }



  render() {

    let top5ODOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 6,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: rowData => { this.odhandleClick(rowData) },
    };

    let top5CompetitorsOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: (rowData, rowIndex) => {
        this.handleCompetitorsClick(rowData)
      },
      onCellClick: (cellData, cellMeta) => {
        this.handleCompetitorsCellClick(cellData, cellMeta)
      }
    };

    let top10AgentsOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: rowData => { this.handleAgentClick(rowData) },
      // onCellClick: (cellIndex, rowData) => { this.cellhandleClick(cellIndex, rowData) },
    };

    $("#pagination-next").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });
    $("#pagination-back").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });

    return (
      <div className='top-market'>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="navdesign">
              <div className="col-md-12 col-sm-12 col-xs-12" style={{ backgroundColor: color.gray }}>
                <section>
                  <h2>TOP MARKETS</h2>
                  {/* <nav>
                    <ol className="cd-breadcrumb">
                      <li onClick={this.homeHandleClick} > POS </li>
                      {bcData.map((item) =>
                        <li onClick={this.listHandleClick} data-value={item.key} id={item.val} title={item.title}> {item.val} </li>
                      )}
                    </ol>
                  </nav> */}
                </section>
              </div>

              {/* <div className="col-md-2 col-sm-2 col-xs-12" style={{ backgroundColor: "#EDEDED" }}>
                <div className="form-group" style={{ marginTop: "14px", marginBottom: "14px" }}>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Cabin</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div> */}

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">

            <div className="x_panel" style={{ marginTop: "15px" }}>
              <div className="x_content">

                {/* <MUIDataTableComponent datatableClassName={'top5ODTable'} data={this.state.top5ODData} columns={this.state.top5ODColumn} options={top5ODOptions} title={this.state.top5ODTitle} /> */}
                <DataTableComponent rowData={this.state.top5ODData} columnDefs={this.state.top5ODColumn} onSelectionChanged={this.onSelectionChanged} rowSelection={`single`} />

              </div>
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="x_panel">
              <div className="x_content">

                <div className="tab" id="posTableTab" role="tabpanel">
                  <ul className="nav nav-tabs" role="tablist">
                    <li role="presentation" className="active" onClick={this.competitorAgentHandleClick} ><a style={{ textTransform: 'initial' }} href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Competitor/Agent</a></li>
                    <li role="presentation" onClick={this.competitorAgentHandleClick}><a style={{ textTransform: 'initial' }} href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">POS</a></li>
                    {/* <li role="presentation" onClick={this.agentHandleClick}><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Agency Flow</a></li> */}
                  </ul>
                  <div className="tab-content tabs">
                    <div role="tabpanel" className="tab-pane fade in active" id="Section1">

                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <MUIDataTableComponent datatableClassName={'top5Competitor'} data={this.state.top5Data} columns={this.state.top5Column} options={top5CompetitorsOptions} title={this.state.tableTitle1} />
                        </div>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <MUIDataTableComponent datatableClassName={'top10Agents'} data={this.state.top10Data} columns={this.state.top10Column} options={top10AgentsOptions} title={this.state.tableTitle2} />
                        </div>
                      </div>

                    </div>

                    <div role="tabpanel" className="tab-pane fade" id="Section2">

                      <div className='row'>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <MUIDataTableComponent datatableClassName={'top5Competitor'} data={this.state.top5Data} columns={this.state.top5Column} options={top5CompetitorsOptions} title={this.state.tableTitle1} />
                        </div>
                        <div className='col-md-6 col-sm-6 col-xs-12'>
                          <MUIDataTableComponent datatableClassName={'top10Agents'} data={this.state.top10Data} columns={this.state.top10Column} options={top10AgentsOptions} title={this.state.tableTitle2} />
                        </div>
                      </div>

                    </div>

                    {/* <div role="tabpanel" className="tab-pane fade" id="Section3">

                      <MUIDataTableComponent datatableClassName={'agencyTable'} data={this.state.posRegionDatas} columns={this.state.regioncolumn} options={odAndAgencyFlowOptions} title={this.state.tableTitle} />

                    </div> */}

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div>
          <ChartModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>

        <div>
          <DatatableModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>


      </div>

    );
  }
}

export default TopMarkets;