import React, { Component } from 'react';
import APIServices from '../../API/apiservices';
import ChartModelDetails from '../../Component/chartModel';
import DatatableModelDetails from '../../Component/dataTableModel';
import MUIDataTableComponent from '../../Component/MuiDataTableComponent';
import color from '../../Constants/color'
import $ from 'jquery';
import '../../App';
import './PosDetails.scss';
import { tree } from 'd3';

const apiServices = new APIServices();

var bcData = [];
var count = 1;
var inputLevel;
var gettingRegion;
var datatableClassName;
const arrowStyle = {
  color: color.green,
  marginLeft: '5px',
  fontSize: '12px'
}

class POSDetail extends Component {


  constructor(props) {
    super(props);
    this.state = {
      // posMonthDetails:[],
      // monthcolumns:[],
      posMonthDetails: [{
        'Month': 'June',
        '': <span className="filter_table_data" id="chartModel" datamonth={'June'} data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': <span className="cabin_table_data" id="cabinModel" datamonth={'June'} data-toggle='modal' data-target="#dataTableModal">1.39M</span>,
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>12<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Month': 'July',
        '': <span className="filter_table_data" id="chartModel" datamonth={'July'} data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': <span className="cabin_table_data" id="cabinModel" datamonth={'July'} data-toggle='modal' data-target="#dataTableModal">1.39M</span>,
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Month': 'August',
        '': <span className="filter_table_data" id="chartModel" datamonth={'August'} data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': <span className="cabin_table_data" id="cabinModel" datamonth={'August'} data-toggle='modal' data-target="#dataTableModal">1.39M</span>,
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Month': 'September',
        '': <span className="filter_table_data" id="chartModel" datamonth={'September'} data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': <span className="cabin_table_data" id="cabinModel" datamonth={'September'} data-toggle='modal' data-target="#dataTableModal">1.39M</span>,
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      },
      {
        'Month': 'October',
        '': <span className="filter_table_data" id="chartModel" datamonth={'October'} data-toggle='modal' data-target="#myModal" style={{ color: "#2a3f54e8" }}><i className="fa fa-bar-chart-o"></i></span>,
        'CY': <span className="cabin_table_data" id="cabinModel" datamonth={'October'} data-toggle='modal' data-target="#dataTableModal">1.39M</span>,
        'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "TKT(%)": '95',
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "FRCT/Act": '1.76M',
        "TGT": '1.83M',
        "VTG(%)": <span>8<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "VLY(%)": <span>6<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
        "CY(%)": '10.48',
        "VLY(pts)": <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
      }
      ],
      monthcolumns: ["Month", "", 'CY', "VLY(%)", "TKT(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "FRCT/Act", "TGT", "VTG(%)", "VLY(%)", "CY(%)", "VLY(pts)"],
      compartmentColumn: ['RBD', 'Booking', 'VLY(%)', 'Ticketed Average Fare(SR)', 'VLY(%)'],
      compartmentData: [
        {
          'RBD': 'A',
          'Booking': '580',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Ticketed Average Fare(SR)': '3099',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'RBD': 'B',
          'Booking': '580',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Ticketed Average Fare(SR)': '3099',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'RBD': 'C',
          'Booking': '580',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Ticketed Average Fare(SR)': '3099',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'RBD': 'D',
          'Booking': '580',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Ticketed Average Fare(SR)': '3099',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'RBD': 'E',
          'Booking': '580',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Ticketed Average Fare(SR)': '3099',
          'VLY(%)': <span>1.2<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
      ],
      regioncolumn: [],
      posRegionDatas: [],
      modelRegionDatas: [],
      modelregioncolumn: [],
      tableDatas: true,
      gettingMonth: new Date().getMonth() + 1,
      monthName: 'January',
      tableTitle: '',
      // monthTableTitle:'',
      monthTableTitle: 'NETWORK',
      tabLevel: 'Null',
      posFlowData: 'Null',
      posAgentFlowDatas: [],
      posAgentFlowcolumn: [],
      cabinOption: [],
      cabinselectedvalue: 'Null',
      toggle: 'lc'
    };

  }

  componentDidMount() {

    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";


    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

    apiServices.getClassNameDetails().then(function (result) {
      var classData = result[0].classDatas;
      self.setState({ cabinOption: classData })
    });

  }

  monthWisehandleClick(rowData, rowIndex) {
    var monththis = this;
    var gettingMonth = window.monthNameToNum(rowData[0]);
    monththis.setState({ monthName: rowData[0] })
    var getColName = $('.regionTable thead tr th:nth-child(1)').text();
    var getCabinValue = this.state.cabinselectedvalue;
    console.log("1 is If", getColName);
    if (getColName === 'RegionName') {
      apiServices.getPOSRegionTables(gettingMonth, getCabinValue).then(function (result) {
        inputLevel = "R";
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      });

    } else if (getColName === 'CountryName') {
      console.log("2 is If")
      inputLevel = "C";
      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      });
    } else if (getColName === 'POS') {
      console.log("3 is If")
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      });

    } else if (getColName === 'CommonOD') {
      console.log("4 is If")
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
      });

    }
    else if (getColName === 'OD') {
      console.log("5 is If")
      apiServices.getODFlowTables(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posODdata = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
      });

    }
    else if (getColName === 'Agent') {
      console.log("6 is If")
      apiServices.getAgentDetails(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posAgentData = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        monththis.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
      });
    }
    // gettingMonth, gettingRegion, gettingRegionCode, getCabinValue
    //   apiServices.getAgentDetails(gettingMonth,inputLevel,posFlowData,getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posAgentData = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posAgentData, regioncolumn:columnName, tableTitle: tableTitle})
    // });
    bcData = [];
  }

  monthWiseCellClick = (cellIndex, rowData) => {
    if (cellIndex.props !== undefined) {
      var getMonth = window.monthNameToNum(cellIndex.props.datamonth);
      this.setState({ monthName: cellIndex.props.datamonth })
    }
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var self = this;
      var getCabinValue = this.state.cabinselectedvalue;
      apiServices.getPOSRegionTables(getMonth, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    } else if (rowData.colIndex === 2) {
      rowData.event.stopPropagation();
      var self = this;
      var inputLevel = this.state.tabLevel;
      var posFlowData = this.state.posFlowData;
      var getCabinValue = this.state.cabinselectedvalue;

      // apiServices.getCabinDetails(getMonth, inputLevel, 'ROW', getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posregiontableDatas;
      //   self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      // });
    }
  }

  regionhandleClick(rowData) {
    var regionthis = this;
    var gettingRegion = rowData[0];
    var gettingMonth = this.state.gettingMonth;
    var tableHead = Object.keys(this.state.posRegionDatas[0])
    var getColName = tableHead[0];
    var getCabinValue = this.state.cabinselectedvalue;

    if (getColName === 'RegionName') {
      inputLevel = "C";

      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;

        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Region" });

        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'R', posFlowData: gettingRegion })
      });

      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });

    } else if (getColName === 'CountryName') {
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;

        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Country" });

        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'C', posFlowData: gettingRegion })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });

    } else if (getColName === 'POS') {
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "POS" });

        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'P', posFlowData: gettingRegion })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });

    } else if (getColName === 'CommonOD') {
      inputLevel = "S";
      apiServices.getPOSClassDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "CommonOD" });
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'Null', posFlowData: 'Null' })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });

    }

    count = 2;
  }

  regionCellClick = (cellIndex, rowData) => {
    var self = this;
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var getCabinValue = self.state.cabinselectedvalue;
      apiServices.getPOSRegionTables(self.state.gettingMonth, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    } else if (rowData.colIndex === 9) {
      self.setState({
        compartmentColumn: ['Cabin', 'Lowest RBD', 'Avail', 'Cabin', 'Lowest RBD', 'Avail'],
        compartmentData: [
          {
            'Cabin': 'F',
            'Lowest RBD': 'A',
            'Avail': ' 400',
            'Cabin': 'F',
            'Lowest RBD': 'A',
            'Avail': ' 400'
          },
          {
            'Cabin': 'J',
            'Lowest RBD': 'A',
            'Avail': ' 400',
            'Cabin': '  J',
            'Lowest RBD': 'A',
            'Avail': ' 400'
          },
          {
            'Cabin': 'Y',
            'Lowest RBD': 'A',
            'Avail': ' 400',
            'Cabin': '  Y',
            'Lowest RBD': 'A',
            'Avail': ' 400'
          },
        ]
      })
    }
  }

  posHandleClick = (e) => {
    var region = this;
    region.setState({ posRegionDatas: [] })

    var inputLevel = this.state.tabLevel;
    var posFlowData = this.state.posFlowData;
    var getCabinValue = this.state.cabinselectedvalue;
    var tabValueChange;

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

    // if (inputLevel === 'R' && posFlowData === 'Null') {
    //   apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posRegionata = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else if (inputLevel === 'R' && posFlowData != 'Null') {
    //   tabValueChange = 'C';
    //   apiServices.getPOSCountryDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });

    // } else if (inputLevel === 'C') {
    //   tabValueChange = 'P';
    //   apiServices.getPOSDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else {
    //   tabValueChange = 'O';
    //   apiServices.getPOSODDetails(this.state.gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     console.log('pos od details', result)
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // }
  }

  odHandleClick = (e) => {
    var region = this;
    region.setState({ posRegionDatas: [] })

    var getCabinValue = this.state.cabinselectedvalue;
    var tabValueChange = this.state.tabLevel;
    var tabValueChange;
    if (this.state.tabLevel === 'R' && this.state.posFlowData === 'Null') {
      tabValueChange = 'R';
    } else if (this.state.tabLevel === 'R' && this.state.posFlowData != 'Null') {
      tabValueChange = 'R';
    } else if (this.state.tabLevel === 'C') {
      tabValueChange = 'C';
    } else if (this.state.tabLevel === 'P') {
      tabValueChange = 'P';
    }
    apiServices.getODFlowTables(this.state.gettingMonth, tabValueChange, this.state.posFlowData, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posODdata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      region.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
    });
    bcData = [];
  }

  agentHandleClick = (e) => {
    var region = this;
    region.setState({ posRegionDatas: [] })

    var getCabinValue = this.state.cabinselectedvalue;
    var gettingMonth = this.state.gettingMonth;
    var inputLevel = this.state.tabLevel;
    var posFlowData = this.state.posFlowData;

    apiServices.getAgentDetails(gettingMonth, inputLevel, posFlowData, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posAgentData = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      region.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
    });
    bcData = [];
  }

  homeHandleClick = (e) => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";
    // apiServices.getPOSMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });
    bcData = [];
  }

  listHandleClick = (e) => {
    var regionthis = this;
    gettingRegion = e.target.id;
    var gettingMonth = this.state.gettingMonth;
    var getColName = e.target.title;
    var getCabinValue = this.state.cabinselectedvalue;

    var indexEnd = bcData.findIndex(function (e) {
      return e.val == gettingRegion;
    })
    var removeArrayIndex = bcData.slice(0, indexEnd + 1);
    bcData = removeArrayIndex;

    if (getColName === 'Region') {
      inputLevel = "C";
      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'Country') {
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'POS') {
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    }

  }

  cabinSelectChange = (e) => {
    var self = this;
    bcData = [];
    var getCabinValue = e.target.value;
    var gettingRegion = 'Null';
    var inputLevel = 'R';

    self.setState({ cabinselectedvalue: e.target.value });

    // apiServices.getMonthWise(gettingRegion, inputLevel, e.target.value).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

  }

  toggle(val) {
    this.setState({ toggle: val })
  }

  render() {

    let options = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: (rowData, rowIndex) => {
        this.monthWisehandleClick(rowData, rowIndex);
        var rowindex = (rowIndex.rowIndex) + 1;
        $(".monthTable tbody tr").css("background-color", "#fff");
        $(".monthTable tbody tr td").css("color", "#2a3f54e8");
        $(".monthTable tbody tr:nth-child(" + rowindex + ")").css("background-color", "#537790c2");
        $(".monthTable tbody tr#MUIDataTableBodyRow-" + rowIndex.dataIndex + " > td").css("color", "#fff")
      },
      onCellClick: (cellIndex, rowData) => {
        this.monthWiseCellClick(cellIndex, rowData)
      }
    };

    let regionOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: rowData => { this.regionhandleClick(rowData) },
      onCellClick: (cellIndex, rowData) => { this.regionCellClick(cellIndex, rowData) },
    };

    let odAndAgencyFlowOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 6,
      rowsPerPageOptions: [6, 25, 50],
    };

    $("#pagination-next").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });
    $("#pagination-back").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });

    return (
      <div className='pos-details'>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12 top">
            <div className="navdesign">
              <div className="col-md-5 col-sm-5 col-xs-6 toggle1">
                <section>
                  <button className={`btn active-btn`} onClick={() => this.props.history.push('/pospage')}>POS</button>
                  <button className='btn' onClick={() => this.props.history.push('/routepage')}>Route</button>
                </section>
              </div>

              <div className="col-md-5 col-sm-5 col-xs-6 toggle2">
                <button className={`btn ${this.state.toggle === 'lc' ? 'active-btn' : ''}`} onClick={() => this.toggle('lc')}>LC($)</button>
                <button className={`btn ${this.state.toggle === 'bc' ? 'active-btn' : ''}`} onClick={() => this.toggle('bc')}>BC(SR)</button>
              </div>

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12" style={{ backgroundColor: color.gray, marginTop: '15px' }}>
            <div className="navdesign">
              <div className="col-md-10 col-sm-10 col-xs-12">
                <section>
                  <nav>
                    <ol className="cd-breadcrumb">
                      <li onClick={this.homeHandleClick} > NETWORK </li>
                      {bcData.map((item) =>
                        <li onClick={this.listHandleClick} data-value={item.key} id={item.val} title={item.title}> {item.val} </li>
                      )}
                    </ol>
                  </nav>
                </section>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <div className="form-group" style={{ marginTop: "14px", marginBottom: "14px" }}>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Cabin</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">

            <div className="x_panel" style={{ marginTop: "15px" }}>
              <div className="x_content">

                <MUIDataTableComponent datatableClassName={'monthTable'} data={this.state.posMonthDetails} columns={this.state.monthcolumns} options={options} title={''} />

              </div>
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="x_panel">
              <div className="x_content">

                <div className="tab" id="posTableTab" role="tabpanel">
                  <ul className="nav nav-tabs" role="tablist">
                    <li role="presentation" className="active" onClick={this.posHandleClick} ><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">POS Flow</a></li>
                    <li role="presentation" onClick={this.odHandleClick}><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">O&D Flow</a></li>
                    <li role="presentation" onClick={this.agentHandleClick}><a href="#Section3" aria-controls="messages" role="tab" data-toggle="tab">Agency Flow</a></li>
                  </ul>
                  <div className="tab-content tabs">
                    <div role="tabpanel" className="tab-pane fade in active" id="Section1">

                      <MUIDataTableComponent datatableClassName={'regionTable'} data={this.state.posRegionDatas} columns={this.state.regioncolumn} options={regionOptions} title={this.state.tableTitle} />

                    </div>

                    <div role="tabpanel" className="tab-pane fade" id="Section2">

                      <MUIDataTableComponent datatableClassName={'odTable'} data={this.state.posRegionDatas} columns={this.state.regioncolumn} options={odAndAgencyFlowOptions} title={this.state.tableTitle} />

                    </div>

                    <div role="tabpanel" className="tab-pane fade" id="Section3">

                      <MUIDataTableComponent datatableClassName={'agencyTable'} data={this.state.posRegionDatas} columns={this.state.regioncolumn} options={odAndAgencyFlowOptions} title={this.state.tableTitle} />

                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div>
          <DatatableModelDetails datas={this.state.compartmentData} columns={this.state.compartmentColumn} title={`NETWORK ${this.state.monthName}`} />
          <ChartModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>

      </div>

    );
  }
}

export default POSDetail;