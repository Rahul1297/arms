import React, { Component } from "react";
import Indicators from "../../Component/indicators";
import Barchart from "./FirstSection/barchart";
import Linechart from "./SecondSection/linechart";
import PivotTable from '../../pivottable';

class Dashboard extends Component {
  render() {
      
    return (
        <div>
            <div className="row tile_count">
                <Indicators />
            </div>  
            <div className="row">
                <Barchart />
            </div>
            <div className="row">
                <Linechart />
            </div>
            {/* <div className="row">
                <PivotTable />
            </div> */}
        </div>
    )
  }
}

export default Dashboard