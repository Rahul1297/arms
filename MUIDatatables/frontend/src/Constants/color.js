const color = {
    primaryColor: "#2a3f54e8",
    secondaryColor: '#2c3f4c',
    green: '#21d521',
    gray:'#EDEDED'
}
export default color;