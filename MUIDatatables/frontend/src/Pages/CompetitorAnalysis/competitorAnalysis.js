import React, { Component } from 'react';
import APIServices from '../../API/apiservices';
import ChartModelDetails from '../../Component/chartModel';
import DatatableModelDetails from '../../Component/dataTableModel';
import MUIDataTableComponent from '../../Component/MuiDataTableComponent';
import color from '../../Constants/color'
import $ from 'jquery';
import '../../App';
import './CompetitorAnalysis.scss';

const apiServices = new APIServices();

var bcData = [];
var count = 1;
var inputLevel;
var gettingRegion;
var datatableClassName;
const arrowStyle = {
  color: color.green,
  marginLeft: '5px',
  fontSize: '12px'
}

class CompetitorAnalysis extends Component {


  constructor(props) {
    super(props);
    this.state = {
      // posMonthDetails:[],
      // monthcolumns:[],
      competitorColumn: ['Cabin', 'MIDT CY Bookings', 'MIDT LY Bookings', 'MIDT Bookings VLY(%)', 'CY MS', 'LY MS', 'MIDT CY Bookings', 'MIDT LY Bookings', 'MIDT Bookings VLY(%)', 'CY MS', 'LY MS'],
      competitorData: [
        {
          'Cabin': 'Economy',
          'MIDT CY Bookings': '0.12M',
          'MIDT LY Bookings': '0.12M',
          'MIDT Bookings VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY MS': '1.86',
          'LY MS': '1.86',
          'MIDT CY Bookings': '0.12M',
          'MIDT LY Bookings': '0.12M',
          'MIDT Bookings VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY MS': '1.86',
          'LY MS': '1.86',
        },
        {
          'Cabin': 'Premium',
          'MIDT CY Bookings': '0.12M',
          'MIDT LY Bookings': '0.12M',
          'MIDT Bookings VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY MS': '1.86',
          'LY MS': '1.86',
          'MIDT CY Bookings': '0.12M',
          'MIDT LY Bookings': '0.12M',
          'MIDT Bookings VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY MS': '1.86',
          'LY MS': '1.86',
        },
        {
          'Cabin': 'Total',
          'MIDT CY Bookings': '0.24M',
          'MIDT LY Bookings': '0.24M',
          'MIDT Bookings VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY MS': '3..6',
          'LY MS': '3.6',
          'MIDT CY Bookings': '0.24M',
          'MIDT LY Bookings': '0.24M',
          'MIDT Bookings VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY MS': '3..6',
          'LY MS': '3.6',
        }
      ],
      topMarketColumn: ['OD', '', 'CY', 'LY(%)', 'VLY(%)', 'CY', 'LY(%)', 'Airline Rank'],
      top10Column: ['Name', 'CY', 'VLY(%)', 'YTD', 'CY', 'VLY(%)', 'YTD'],
      topMarketData: [
        {
          'OD': 'HKG-LHR1',
          '': 'CX',
          'CY': '26786',
          'LY(%)': '23334',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '43.6',
          'LY(%)': '76',
          'Airline Rank': 'CX,BX,DB,RH,AD(7)'
        },
        {
          'OD': 'HKG-LHR2',
          '': 'CX',
          'CY': '26786',
          'LY(%)': '23334',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '43.6',
          'LY(%)': '76',
          'Airline Rank': 'CX,BX,DB,RH,AD(7)'
        },
        {
          'OD': 'HKG-LHR3',
          '': 'CX',
          'CY': '26786',
          'LY(%)': '23334',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '43.6',
          'LY(%)': '76',
          'Airline Rank': 'CX,BX,DB,RH,AD(7)'
        }
      ],
      top10Data: [
        {
          'Name': 'OMEGA_TRAVELS_LTD1',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
        },
        {
          'Name': 'OMEGA_TRAVELS_LTD2',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
        },
        {
          'Name': 'OMEGA_TRAVELS_LTD3',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
          'CY': '358',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'YTD': '60',
        }
      ],
      odName: '',
      modelRegionDatas: [],
      modelregioncolumn: [],
      tableDatas: true,
      gettingMonth: new Date().getMonth() + 1,
      tableTitle1: '',
      tableTitle2: '',
      // monthTableTitle:'',
      top5ODTitle: 'TOP 5 ODs',
      tabLevel: 'Null',
      posFlowData: 'Null',
      posAgentFlowDatas: [],
      posAgentFlowcolumn: [],
      cabinOption: [],
      cabinselectedvalue: 'Null',
      competitorName: '',
      odName: ''
    };

  }

  componentDidMount() {
    var competitorName = window.localStorage.getItem('Competitor')
    var odName = this.state.topMarketData[0].OD
    this.setState({
      competitorName: competitorName,
      odName: odName,
      tableTitle1: `Top Markets for ${competitorName}`,
      tableTitle2: `Top 10 Agents for ${competitorName} & ${odName}`,
    })
    // var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // apiServices.getClassNameDetails().then(function (result) {
    //   var classData = result[0].classDatas;
    //   self.setState({ cabinOption: classData })
    // });

  }

  // handleClick(rowData) {
  //   var monththis = this;
  //   var gettingMonth = window.monthNameToNum(rowData[0].props.children);
  //   var getColName = $('.regionTable thead tr th:nth-child(1)').text();
  //   var getCabinValue = this.state.cabinselectedvalue;

  //   if (getColName === 'RegionName') {
  //     console.log("1 is If", getColName);
  //     apiServices.getPOSRegionTables(gettingMonth, getCabinValue).then(function (result) {
  //       inputLevel = "R";
  //       var columnName = result[0].columnName;
  //       var posRegionata = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   } else if (getColName === 'CountryName') {
  //     console.log("2 is If")
  //     inputLevel = "C";
  //     apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posCountrytableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });
  //   } else if (getColName === 'POS') {
  //     console.log("3 is If")
  //     inputLevel = "P";
  //     apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   } else if (getColName === 'CommonOD') {
  //     console.log("4 is If")
  //     inputLevel = "O";
  //     apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   }
  //   else if (getColName === 'OD') {
  //     console.log("5 is If")
  //     apiServices.getODFlowTables(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posODdata = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
  //     });

  //   }
  //   else if (getColName === 'Agent') {
  //     console.log("6 is If")
  //     apiServices.getAgentDetails(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posAgentData = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
  //     });

  //   }
  //   // gettingMonth, gettingRegion, gettingRegionCode, getCabinValue
  //   //   apiServices.getAgentDetails(gettingMonth,inputLevel,posFlowData,getCabinValue).then(function (result) {
  //   //     var columnName = result[0].columnName;
  //   //     var posAgentData = result[0].posregiontableDatas;
  //   //     var tableTitle = result[0].tableTitle;
  //   //     region.setState({ posRegionDatas: posAgentData, regioncolumn:columnName, tableTitle: tableTitle})
  //   // });
  //   bcData = [];
  // }

  odhandleClick(rowData) {

    var gettingOD = rowData[0];
    this.setState({
      tableTitle1: `Top 5 Competitors for ${gettingOD}`,
      tableTitle2: `Top 10 Agents for ${gettingOD}`,
      odName: gettingOD
    })

    // var gettingMonth = this.state.gettingMonth;
    // var getColName = $('.regionTable thead tr th:nth-child(1)').text();
    // var getCabinValue = this.state.cabinselectedvalue;

    // if (getColName === 'RegionName') {
    //   inputLevel = "C";
    //   apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;

    //     bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Region" });

    //     regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'R', posFlowData: gettingRegion })
    //   });

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });
    // }
  }

  competitorAgentHandleClick = (e) => {

    this.setState({ tableTitle1: `Top 5 Competitors for ${this.state.odName}`, tableTitle2: `Top 10 Agents for ${this.state.odName}` })

    // var region = this;
    // region.setState({ posRegionDatas: [] })

    // var inputLevel = this.state.tabLevel;
    // var posFlowData = this.state.posFlowData;
    // var getCabinValue = this.state.cabinselectedvalue;
    // var tabValueChange;

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // if (inputLevel === 'R' && posFlowData === 'Null') {
    //   apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posRegionata = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else if (inputLevel === 'R' && posFlowData != 'Null') {
    //   tabValueChange = 'C';
    //   apiServices.getPOSCountryDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });

    // } else if (inputLevel === 'C') {
    //   tabValueChange = 'P';
    //   apiServices.getPOSDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else {
    //   tabValueChange = 'O';
    //   apiServices.getPOSODDetails(this.state.gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     console.log('pos od details', result)
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // }
  }

  handleTopMarketClick = (rowData) => {
    var odName = rowData[0];
    this.setState({
      odName: odName,
      tableTitle2: `Top 10 Agents for ${this.state.competitorName} & ${odName}`
    })
  }

  handleAgentClick = (rowData) => {
    window.localStorage.setItem('Agent', rowData[0])
    this.props.history.push('/agentAnalysis')
  }

  cellhandleClick = (cellIndex, rowData) => {
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var self = this;
      var getCabinValue = this.state.cabinselectedvalue;
      apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    }
  }

  cabinSelectChange = (e) => {
    var self = this;
    bcData = [];
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = 'Null';
    var inputLevel = 'R';

    self.setState({ cabinselectedvalue: e.target.value });

    // apiServices.getMonthWise(gettingRegion, inputLevel, e.target.value).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, e.target.value).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

  }

  render() {

    let competitorsOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 6,
      rowsPerPageOptions: [6, 25, 50],
    };

    let topMarketOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: (rowData, rowIndex) => {
        this.handleTopMarketClick(rowData)
      },
    };

    let top10AgentsOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: rowData => { this.handleAgentClick(rowData) }
    };

    $("#pagination-next").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });
    $("#pagination-back").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });

    return (
      <div className='competitor-analysis'>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12 top-section">
            <div className="navdesign">
              <div className="">
                <button className='btn' onClick={() => this.props.history.push('/home')}><i className='fa fa-caret-left back'></i>Back To Home</button>
                {/* <nav>
                    <ol className="cd-breadcrumb">
                      <li onClick={this.homeHandleClick} > POS </li>
                      {bcData.map((item) =>
                        <li onClick={this.listHandleClick} data-value={item.key} id={item.val} title={item.title}> {item.val} </li>
                      )}
                    </ol>
                  </nav> */}
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Region:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Region</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>POS:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select POS</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Competitor Airline *:</h4>
                  <input style={{ marginLeft: '10px' }} type='text' className="form-control cabinselect"
                    value={this.state.competitorName}
                    onChange={(competitorName) => this.setState({ competitorName })}></input>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Travel Month:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Month</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Top Market:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Market</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="">
                <button className='btn'><i className='fa fa-search back'></i>Search</button>
              </div>

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">

            <div className="x_panel" style={{ marginTop: "15px" }}>
              <div className="x_content">

                <MUIDataTableComponent datatableClassName={'top5ODTable'} data={this.state.competitorData} columns={this.state.competitorColumn} options={competitorsOptions} title={''} />

              </div>
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="x_panel">
              <div className="x_content">
                <div className='row'>
                  <div className='col-md-6 col-sm-6 col-xs-12'>
                    <MUIDataTableComponent datatableClassName={'top5Competitor'} data={this.state.topMarketData} columns={this.state.topMarketColumn} options={topMarketOptions} title={this.state.tableTitle1} />
                  </div>
                  <div className='col-md-6 col-sm-6 col-xs-12'>
                    <MUIDataTableComponent datatableClassName={'top10Agents'} data={this.state.top10Data} columns={this.state.top10Column} options={top10AgentsOptions} title={this.state.tableTitle2} />
                  </div>
                </div>

              </div>
            </div>

            {/* <div role="tabpanel" className="tab-pane fade" id="Section3">

                      <MUIDataTableComponent datatableClassName={'agencyTable'} data={this.state.posRegionDatas} columns={this.state.regioncolumn} options={odAndAgencyFlowOptions} title={this.state.tableTitle} />

                    </div> */}

          </div>
        </div>

        <div>
          <ChartModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>

        <div>
          <DatatableModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>


      </div >

    );
  }
}

export default CompetitorAnalysis;