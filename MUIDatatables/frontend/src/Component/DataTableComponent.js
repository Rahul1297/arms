import React, { Component } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import '../ag-grid-table.scss';
import Spinners from "../spinneranimation";

class DataTableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultColDef: {
                sortable: true,
                resizable: true,
                filter: true
            },
        }
    }


    render() {
        if (!this.props.rowData.length) {
            return (
                <Spinners />)
        } else {
            return (
                <div
                    className="ag-theme-balham ag-grid-table"
                    style={{
                        height: '250px',
                        width: '100vw'
                    }}
                >
                    <AgGridReact
                        columnDefs={this.props.columnDefs}
                        rowData={this.props.rowData}
                        defaultColDef={this.state.defaultColDef}
                        // onSelectionChanged={this.props.onSelectionChanged}
                        rowSelection={this.props.rowSelection}
                    >
                    </AgGridReact>
                </div>
            );
        }

    }
}

export default DataTableComponent;
