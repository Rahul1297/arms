import React from "react";
import { Route } from "react-router-dom";
import LeftNavheader from './leftnavheader';
import Login from "./Pages/Login/login";
import POSDetail from "./Pages/PosDetails/posDetails";
import Routes from "./Pages/Route/route";
import sample from './Pages/Sample/sample';
import TopMarkets from './Pages/TopMarkets/topMarkets';
import CompetitorAnalysis from './Pages/CompetitorAnalysis/competitorAnalysis';
import AgentAnalysis from './Pages/AgentAnalysis/agentAnalysis';
import ChannelPerformance from './Pages/ChannelPerformance/channelPerformance';
import SubChannelPerformance from './Pages/SubChannelPerformance/subChannelPerformance';

const BaseRouter = () => (
  <div>
    <Route exact path="/" component={Login} />
    <Route exact path="/home" component={LeftNavheader} />
    <Route path="/pospage" component={POSDetail} />
    <Route path="/routepage" component={Routes} />
    <Route path="/topMarkets" component={TopMarkets} />
    <Route path="/competitorAnalysis" component={CompetitorAnalysis} />
    <Route path="/agentAnalysis" component={AgentAnalysis} />
    <Route path="/channelPerformance" component={ChannelPerformance} />
    <Route path="/subChannelPerformance" component={SubChannelPerformance} />
    <Route path="/sample" component={sample} />
  </div>
);

export default BaseRouter;