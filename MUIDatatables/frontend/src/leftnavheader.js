import React, { Component } from 'react';
import APIServices from './API/apiservices';
import $ from 'jquery';

import { Route, NavLink, HashRouter } from "react-router-dom";

import POSDetail from "./Pages/PosDetails/posDetails";
import Routes from "./Pages/Route/route";
import Dashboard from "./Pages/Dashboard/dashboard"

const apiServices = new APIServices();

class LeftNavheader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userDetails: []
    };

  }
  componentDidMount() {
    var self = this;

    // apiServices.getPOSDetails().then(function (result) {
    //   console.log("Happy : ", result);
    //   self.setState({ posDetails: result})
    // });

  }

  render() {
    return (

      <div className="container body">
        <div className="main_container">
          <Headernav />
          <LeftSidemenu />
        </div>
      </div>
    );
  }
}


class Headernav extends React.Component {

  sidemenuToggle = () => {

    if ($('body').hasClass('nav-md')) {
      $('#sidebar-menu').find('li.active ul').hide();
      $('#sidebar-menu').find('li.active').addClass('active-sm').removeClass('active');
    } else {
      $('#sidebar-menu').find('li.active-sm ul').show();
      $('#sidebar-menu').find('li.active-sm').addClass('active').removeClass('active-sm');
    }

    $('body').toggleClass('nav-md nav-sm');

  };

  render() {
    return (

      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand" href="#">RevMax</a>
          </div>
          <div id="navbar" className="navbar-collapse collapse">
            <ul className="nav navbar-nav navbar-right">
              <li className="active"><a href="http://localhost:3000/Admins">Portal</a></li>
              <li ><a href="#">Rahul K</a></li>
              <li><a href="/">Logout</a></li>
              {/* <li className="dropdown">
                  <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span className="caret"></span></a>
                  <ul className="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" className="divider"></li>
                    <li className="dropdown-header">Nav header</li>
                    <li><a href="#">Separated link</a></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li> */}
            </ul>
          </div>
        </div>
      </nav>


    );
  }
}

class LeftSidemenu extends React.Component {

  render() {
    return (
      <HashRouter>
        <div className="right_col" role="main">
          <Route exact path="/" component={Dashboard} />
          <Route path="/pospage" component={POSDetail} />
          <Route path="/routepage" component={Routes} />
        </div>

        <footer>
          <div className="pull-right">
            <a href="http://pathfinder.global/" target="_blank">Pathfinder Global FZCO</a>
          </div>
          <div className="clearfix"></div>
        </footer>

      </HashRouter>
    );
  }
}

export default LeftNavheader;