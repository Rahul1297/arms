import React, { Component } from 'react';
import APIServices from '../../API/apiservices';
import ChartModelDetails from '../../Component/chartModel';
import DatatableModelDetails from '../../Component/dataTableModel';
import MUIDataTableComponent from '../../Component/MuiDataTableComponent';
import color from '../../Constants/color'
import $ from 'jquery';
import '../../App';
import './ChannelPerformance.scss';

const apiServices = new APIServices();

var bcData = [];
var count = 1;
var inputLevel;
var gettingRegion;
var datatableClassName;
const arrowStyle = {
  color: color.green,
  marginLeft: '5px',
  fontSize: '12px'
}

class ChannelPerformance extends Component {


  constructor(props) {
    super(props);
    this.state = {
      // posMonthDetails:[],
      // monthcolumns:[],
      channelColumn: ['Channel', 'CY', 'VLY(%)', 'TKT(%)', 'Forecast', 'TGT', 'VTG', 'VLY', 'Forecast', 'TGT', 'VTG', 'VLY', 'Forecast', 'TGT', 'VTG', 'VLY', 'Market Share', 'VLY', 'SA Share', 'VLY'],
      channelData: [
        {
          'Channel': 'Global OTAs',
          'CY': '0.46',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'TKT(%)': '56',
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Market Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'SA Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'Channel': 'Global OTAs',
          'CY': '0.46',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'TKT(%)': '56',
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Market Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'SA Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'Channel': 'Global OTAs',
          'CY': '0.46',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'TKT(%)': '56',
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Market Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'SA Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        },
        {
          'Channel': 'Global OTAs',
          'CY': '0.46',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'TKT(%)': '56',
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Forecast': '1.54M',
          'TGT': '1.44M',
          'VTG': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'Market Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'SA Share': '9.55',
          'VLY': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>
        }
      ],
      odName: '',
      modelRegionDatas: [],
      modelregioncolumn: [],
      tableDatas: true,
      gettingMonth: new Date().getMonth() + 1,
      tableTitle1: '',
      tableTitle2: '',
      // monthTableTitle:'',
      top5ODTitle: 'TOP 5 ODs',
      tabLevel: 'Null',
      posFlowData: 'Null',
      posAgentFlowDatas: [],
      posAgentFlowcolumn: [],
      cabinOption: [],
      cabinselectedvalue: 'Null',
    };

  }

  componentDidMount() {
    this.setState({
      tableTitle1: `Top Markets for`,
      tableTitle2: `Top 10 Agents for`,
    })
    // var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // apiServices.getClassNameDetails().then(function (result) {
    //   var classData = result[0].classDatas;
    //   self.setState({ cabinOption: classData })
    // });

  }


  handleChannelClick = (rowData) => {
    this.props.history.push('/subChannelPerformance')
  }

  // odHandleClick = (e) => {
  //   var region = this;
  //   region.setState({ posRegionDatas: [] })

  //   var getCabinValue = this.state.cabinselectedvalue;
  //   var tabValueChange = this.state.tabLevel;
  //   var tabValueChange;
  //   if (this.state.tabLevel === 'R' && this.state.posFlowData === 'Null') {
  //     tabValueChange = 'R';
  //   } else if (this.state.tabLevel === 'R' && this.state.posFlowData != 'Null') {
  //     tabValueChange = 'R';
  //   } else if (this.state.tabLevel === 'C') {
  //     tabValueChange = 'C';
  //   } else if (this.state.tabLevel === 'P') {
  //     tabValueChange = 'P';
  //   }
  //   apiServices.getODFlowTables(this.state.gettingMonth, tabValueChange, this.state.posFlowData, getCabinValue).then(function (result) {
  //     var columnName = result[0].columnName;
  //     var posODdata = result[0].posregiontableDatas;
  //     var tableTitle = result[0].tableTitle;
  //     region.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
  //   });
  // }

  // agentHandleClick = (e) => {
  //   var region = this;
  //   region.setState({ posRegionDatas: [] })

  //   var getCabinValue = this.state.cabinselectedvalue;
  //   var gettingMonth = this.state.gettingMonth;
  //   var inputLevel = this.state.tabLevel;
  //   var posFlowData = this.state.posFlowData;

  //   apiServices.getAgentDetails(gettingMonth, inputLevel, posFlowData, getCabinValue).then(function (result) {
  //     var columnName = result[0].columnName;
  //     var posAgentData = result[0].posregiontableDatas;
  //     var tableTitle = result[0].tableTitle;
  //     region.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
  //   });
  // }

  homeHandleClick = (e) => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";
    // apiServices.getPOSMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });
    bcData = [];
  }

  listHandleClick = (e) => {
    var regionthis = this;
    gettingRegion = e.target.id;
    var gettingMonth = this.state.gettingMonth;
    var getColName = e.target.title;
    var getCabinValue = this.state.cabinselectedvalue;

    var indexEnd = bcData.findIndex(function (e) {
      return e.val == gettingRegion;
    })
    var removeArrayIndex = bcData.slice(0, indexEnd + 1);
    bcData = removeArrayIndex;

    if (getColName === 'Region') {
      inputLevel = "C";
      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'Country') {
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'POS') {
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    }

  }

  cellhandleClick = (cellIndex, rowData) => {
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var self = this;
      var getCabinValue = this.state.cabinselectedvalue;
      apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    }
  }

  cabinSelectChange = (e) => {
    var self = this;
    bcData = [];
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = 'Null';
    var inputLevel = 'R';

    self.setState({ cabinselectedvalue: e.target.value });

    // apiServices.getMonthWise(gettingRegion, inputLevel, e.target.value).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, e.target.value).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

  }

  render() {

    let channelOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: (rowData, rowIndex) => {
        this.handleChannelClick(rowData)
      },
    };

    $("#pagination-next").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });
    $("#pagination-back").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });

    return (
      <div className='channel-performance'>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12 top-section">
            <div className="navdesign">
              <div style={{ display: 'flex', alignItems: 'center' }} className="col-md-5 col-sm-5 col-xs-12">
                <button className='btn' onClick={() => this.props.history.push('/home')}><i className='fa fa-caret-left back'></i>Back To Home</button>
                <nav>
                  <ol className="cd-breadcrumb">
                    <li onClick={this.homeHandleClick} > POS </li>
                    {bcData.map((item) =>
                      <li onClick={this.listHandleClick} data-value={item.key} id={item.val} title={item.title}> {item.val} </li>
                    )}
                  </ol>
                </nav>
              </div>

              <div className="col-md-1 col-sm-1 col-xs-6" >
                <div className="form-group">
                  <h4>POS:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">ALL</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-1 col-sm-1 col-xs-6" >
                <div className="form-group">
                  <h4>Region:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">ALL</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-1 col-sm-1 col-xs-6" >
                <div className="form-group">
                  <h4>Country:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">ALL</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-1 col-sm-1 col-xs-6" >
                <div className="form-group">
                  <h4>Month:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">ALL</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-1 col-sm-1 col-xs-6" >
                <div className="form-group">
                  <h4>Cabin:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">ALL</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-1 col-sm-1 col-xs-6">
                <button className='btn'><i className='fa fa-search back'></i>Search</button>
              </div>

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">

            <div className="x_panel" style={{ marginTop: "15px" }}>
              <div className="x_content">

                <MUIDataTableComponent datatableClassName={'top5ODTable'} data={this.state.channelData} columns={this.state.channelColumn} options={channelOptions} title={''} />

              </div>
            </div>

          </div>
        </div>

        <div>
          <ChartModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>

        <div>
          <DatatableModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>


      </div >

    );
  }
}

export default ChannelPerformance;