import React, { Component } from 'react';
import APIServices from '../../API/apiservices';
import ChartModelDetails from '../../Component/chartModel';
import DatatableModelDetails from '../../Component/dataTableModel';
import MUIDataTableComponent from '../../Component/MuiDataTableComponent';
import color from '../../Constants/color'
import $ from 'jquery';
import '../../App';
import './AgentAnalysis.scss';

const apiServices = new APIServices();

var bcData = [];
var count = 1;
var inputLevel;
var gettingRegion;
var datatableClassName;
const arrowStyle = {
  color: color.green,
  marginLeft: '5px',
  fontSize: '12px'
}

class AgentAnalysis extends Component {


  constructor(props) {
    super(props);
    this.state = {
      // posMonthDetails:[],
      // monthcolumns:[],
      agentColumn: ['AgentName', 'CY', 'LY(%)', 'VLY(%)', 'CY', 'LY(%)', 'VLY(%)', 'CY(%)', 'YOY(%)', 'AL(%)', 'YOY(pts)'],
      agentData: [
        {
          'AgentName': 'OMEGA_TRAVELS_LTD',
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY(%)': '0.06',
          'YOY(%)': '2',
          'AL(%)': '8',
          'YOY(pts)': '6'
        }
      ],
      topMarketColumn: ['OD', 'CY', 'LY(%)', 'VLY(%)', 'CY', 'LY(%)', 'VLY(%)', 'CY(%)', 'YOY(%)', 'AL(%)', 'YOY(pts)'],
      topMarketData: [
        {
          'OD': 'LHR',
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY(%)': '0.06',
          'YOY(%)': '2',
          'AL(%)': '8',
          'YOY(pts)': '6'
        },
        {
          'OD': 'RER',
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY(%)': '0.06',
          'YOY(%)': '2',
          'AL(%)': '8',
          'YOY(pts)': '6'
        },
        {
          'OD': 'VHR',
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY': '3456',
          'LY(%)': '3566',
          'VLY(%)': <span>10<i style={arrowStyle} className="fa fa-arrow-up"></i></span>,
          'CY(%)': '0.06',
          'YOY(%)': '2',
          'AL(%)': '8',
          'YOY(pts)': '6'
        }
      ],
      odName: '',
      modelRegionDatas: [],
      modelregioncolumn: [],
      tableDatas: true,
      gettingMonth: new Date().getMonth() + 1,
      tableTitle1: '',
      tableTitle2: '',
      // monthTableTitle:'',
      top5ODTitle: 'TOP 5 ODs',
      tabLevel: 'Null',
      posFlowData: 'Null',
      posAgentFlowDatas: [],
      posAgentFlowcolumn: [],
      cabinOption: [],
      cabinselectedvalue: 'Null',
      agentName: ''
    };

  }

  componentDidMount() {
    var agentName = window.localStorage.getItem('Agent')
    this.setState({
      agentName: agentName,
      tableTitle1: `Top Markets for ${agentName}`,
      tableTitle2: `Top 10 Agents for ${agentName}`,
    })
    // var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // apiServices.getClassNameDetails().then(function (result) {
    //   var classData = result[0].classDatas;
    //   self.setState({ cabinOption: classData })
    // });

  }

  // handleClick(rowData) {
  //   var monththis = this;
  //   var gettingMonth = window.monthNameToNum(rowData[0].props.children);
  //   var getColName = $('.regionTable thead tr th:nth-child(1)').text();
  //   var getCabinValue = this.state.cabinselectedvalue;

  //   if (getColName === 'RegionName') {
  //     console.log("1 is If", getColName);
  //     apiServices.getPOSRegionTables(gettingMonth, getCabinValue).then(function (result) {
  //       inputLevel = "R";
  //       var columnName = result[0].columnName;
  //       var posRegionata = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   } else if (getColName === 'CountryName') {
  //     console.log("2 is If")
  //     inputLevel = "C";
  //     apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posCountrytableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });
  //   } else if (getColName === 'POS') {
  //     console.log("3 is If")
  //     inputLevel = "P";
  //     apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   } else if (getColName === 'CommonOD') {
  //     console.log("4 is If")
  //     inputLevel = "O";
  //     apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posDatas = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, gettingMonth: gettingMonth, tableTitle: tableTitle })
  //     });

  //   }
  //   else if (getColName === 'OD') {
  //     console.log("5 is If")
  //     apiServices.getODFlowTables(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posODdata = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
  //     });

  //   }
  //   else if (getColName === 'Agent') {
  //     console.log("6 is If")
  //     apiServices.getAgentDetails(gettingMonth, 'Null', 'Null', getCabinValue).then(function (result) {
  //       var columnName = result[0].columnName;
  //       var posAgentData = result[0].posregiontableDatas;
  //       var tableTitle = result[0].tableTitle;
  //       monththis.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
  //     });

  //   }
  //   // gettingMonth, gettingRegion, gettingRegionCode, getCabinValue
  //   //   apiServices.getAgentDetails(gettingMonth,inputLevel,posFlowData,getCabinValue).then(function (result) {
  //   //     var columnName = result[0].columnName;
  //   //     var posAgentData = result[0].posregiontableDatas;
  //   //     var tableTitle = result[0].tableTitle;
  //   //     region.setState({ posRegionDatas: posAgentData, regioncolumn:columnName, tableTitle: tableTitle})
  //   // });
  //   bcData = [];
  // }

  odhandleClick(rowData) {

    var gettingOD = rowData[0];
    this.setState({
      tableTitle1: `Top 5 Competitors for ${gettingOD}`,
      tableTitle2: `Top 10 Agents for ${gettingOD}`,
      odName: gettingOD
    })

    // var gettingMonth = this.state.gettingMonth;
    // var getColName = $('.regionTable thead tr th:nth-child(1)').text();
    // var getCabinValue = this.state.cabinselectedvalue;

    // if (getColName === 'RegionName') {
    //   inputLevel = "C";
    //   apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;

    //     bcData.push({ "key": inputLevel, "val": gettingRegion, "title": "Region" });

    //     regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle, tabLevel: 'R', posFlowData: gettingRegion })
    //   });

    // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });
    // }
  }

  competitorAgentHandleClick = (e) => {

    this.setState({ tableTitle1: `Top 5 Competitors for ${this.state.odName}`, tableTitle2: `Top 10 Agents for ${this.state.odName}` })

    // var region = this;
    // region.setState({ posRegionDatas: [] })

    // var inputLevel = this.state.tabLevel;
    // var posFlowData = this.state.posFlowData;
    // var getCabinValue = this.state.cabinselectedvalue;
    // var tabValueChange;

    // apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posregiontableDatas;
    //   var tableTitle = result[0].tableTitle;
    //   region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    // });

    // if (inputLevel === 'R' && posFlowData === 'Null') {
    //   apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posRegionata = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else if (inputLevel === 'R' && posFlowData != 'Null') {
    //   tabValueChange = 'C';
    //   apiServices.getPOSCountryDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posCountrytableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });

    // } else if (inputLevel === 'C') {
    //   tabValueChange = 'P';
    //   apiServices.getPOSDetails(this.state.gettingMonth, posFlowData, getCabinValue).then(function (result) {
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // } else {
    //   tabValueChange = 'O';
    //   apiServices.getPOSODDetails(this.state.gettingMonth, gettingRegion, getCabinValue).then(function (result) {
    //     console.log('pos od details', result)
    //     var columnName = result[0].columnName;
    //     var posDatas = result[0].posregiontableDatas;
    //     var tableTitle = result[0].tableTitle;
    //     region.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
    //   });
    // }
  }

  handleCompetitorsClick = (rowData) => {
    console.log('Row data', rowData)
  }

  // odHandleClick = (e) => {
  //   var region = this;
  //   region.setState({ posRegionDatas: [] })

  //   var getCabinValue = this.state.cabinselectedvalue;
  //   var tabValueChange = this.state.tabLevel;
  //   var tabValueChange;
  //   if (this.state.tabLevel === 'R' && this.state.posFlowData === 'Null') {
  //     tabValueChange = 'R';
  //   } else if (this.state.tabLevel === 'R' && this.state.posFlowData != 'Null') {
  //     tabValueChange = 'R';
  //   } else if (this.state.tabLevel === 'C') {
  //     tabValueChange = 'C';
  //   } else if (this.state.tabLevel === 'P') {
  //     tabValueChange = 'P';
  //   }
  //   apiServices.getODFlowTables(this.state.gettingMonth, tabValueChange, this.state.posFlowData, getCabinValue).then(function (result) {
  //     var columnName = result[0].columnName;
  //     var posODdata = result[0].posregiontableDatas;
  //     var tableTitle = result[0].tableTitle;
  //     region.setState({ posRegionDatas: posODdata, regioncolumn: columnName, tableTitle: tableTitle })
  //   });
  // }

  // agentHandleClick = (e) => {
  //   var region = this;
  //   region.setState({ posRegionDatas: [] })

  //   var getCabinValue = this.state.cabinselectedvalue;
  //   var gettingMonth = this.state.gettingMonth;
  //   var inputLevel = this.state.tabLevel;
  //   var posFlowData = this.state.posFlowData;

  //   apiServices.getAgentDetails(gettingMonth, inputLevel, posFlowData, getCabinValue).then(function (result) {
  //     var columnName = result[0].columnName;
  //     var posAgentData = result[0].posregiontableDatas;
  //     var tableTitle = result[0].tableTitle;
  //     region.setState({ posRegionDatas: posAgentData, regioncolumn: columnName, tableTitle: tableTitle })
  //   });
  // }

  homeHandleClick = (e) => {
    var self = this;
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = "Null";
    var inputLevel = "Null";
    // apiServices.getPOSMonthTables(gettingRegion, inputLevel, getCabinValue).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posMonthdata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posMonthdata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });
    bcData = [];
  }

  listHandleClick = (e) => {
    var regionthis = this;
    gettingRegion = e.target.id;
    var gettingMonth = this.state.gettingMonth;
    var getColName = e.target.title;
    var getCabinValue = this.state.cabinselectedvalue;

    var indexEnd = bcData.findIndex(function (e) {
      return e.val == gettingRegion;
    })
    var removeArrayIndex = bcData.slice(0, indexEnd + 1);
    bcData = removeArrayIndex;

    if (getColName === 'Region') {
      inputLevel = "C";
      apiServices.getPOSCountryDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posCountrytableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'Country') {
      inputLevel = "P";
      apiServices.getPOSDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    } else if (getColName === 'POS') {
      inputLevel = "O";
      apiServices.getPOSODDetails(gettingMonth, gettingRegion, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posDatas = result[0].posregiontableDatas;
        var tableTitle = result[0].tableTitle;
        regionthis.setState({ posRegionDatas: posDatas, regioncolumn: columnName, tableTitle: tableTitle })
      });
      // apiServices.getMonthWise(gettingRegion, inputLevel, getCabinValue).then(function (result) {
      //   var columnName = result[0].columnName;
      //   var posRegionata = result[0].posmonthtableDatas;
      //   var tableTitle = result[0].monthTableTitle;
      //   regionthis.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
      // });
    }

  }

  cellhandleClick = (cellIndex, rowData) => {
    if (rowData.colIndex === 1) {
      rowData.event.stopPropagation();
      var self = this;
      var getCabinValue = this.state.cabinselectedvalue;
      apiServices.getPOSRegionTables(this.state.gettingMonth, getCabinValue).then(function (result) {
        var columnName = result[0].columnName;
        var posRegionata = result[0].posregiontableDatas;
        self.setState({ modelRegionDatas: posRegionata, modelregioncolumn: columnName })
      });
    }
  }

  cabinSelectChange = (e) => {
    var self = this;
    bcData = [];
    var getCabinValue = this.state.cabinselectedvalue;
    var gettingRegion = 'Null';
    var inputLevel = 'R';

    self.setState({ cabinselectedvalue: e.target.value });

    // apiServices.getMonthWise(gettingRegion, inputLevel, e.target.value).then(function (result) {
    //   var columnName = result[0].columnName;
    //   var posRegionata = result[0].posmonthtableDatas;
    //   var tableTitle = result[0].monthTableTitle;
    //   self.setState({ posMonthDetails: posRegionata, monthcolumns: columnName, monthTableTitle: tableTitle })
    // });

    apiServices.getPOSRegionTables(this.state.gettingMonth, e.target.value).then(function (result) {
      var columnName = result[0].columnName;
      var posRegionata = result[0].posregiontableDatas;
      var tableTitle = result[0].tableTitle;
      self.setState({ posRegionDatas: posRegionata, regioncolumn: columnName, tableTitle: tableTitle })
    });

  }

  render() {

    let topMarketOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 3,
      rowsPerPageOptions: [6, 25, 50],
      onRowClick: (rowData, rowIndex) => {
        this.handleCompetitorsClick(rowData)
      },
    };

    let agentOptions = {
      responsive: 'scroll',
      filter: false,
      download: true,
      print: false,
      selectableRows: 'none',
      filterType: 'dropdown',
      rowsPerPage: 6,
      rowsPerPageOptions: [6, 25, 50],
    };

    $("#pagination-next").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });
    $("#pagination-back").on("click", function () {
      $(".monthTable tbody tr").css("background-color", "#fff");
      $(".monthTable tbody tr td").css("color", "#000");
    });

    return (
      <div className='agent-analysis'>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12 top-section">
            <div className="navdesign">
              <div className="">
                <button className='btn' onClick={() => this.props.history.push('/home')}><i className='fa fa-caret-left back'></i>Back To Home</button>
                {/* <nav>
                    <ol className="cd-breadcrumb">
                      <li onClick={this.homeHandleClick} > POS </li>
                      {bcData.map((item) =>
                        <li onClick={this.listHandleClick} data-value={item.key} id={item.val} title={item.title}> {item.val} </li>
                      )}
                    </ol>
                  </nav> */}
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Region:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Region</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>POS:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select POS</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Agent Name *:</h4>
                  <input style={{ marginLeft: '10px' }} type='text' className="form-control cabinselect"
                    value={this.state.agentName}
                    onChange={(agentName) => this.setState({ agentName })}></input>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Travel Month:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Month</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Cabin:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Cabin</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12" >
                <div className="form-group">
                  <h4>Top Market:</h4>
                  <select className="form-control cabinselect" onChange={this.cabinSelectChange}>
                    <option selected={true} disabled="disabled" value="">Select Market</option>
                    {this.state.cabinOption.map((item) =>
                      <option key={item.ClassValue} value={item.ClassValue}>{item.ClassText}</option>
                    )}
                  </select>
                </div>
              </div>

              <div className="">
                <button className='btn'><i className='fa fa-search back'></i>Search</button>
              </div>

            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">

            <div className="x_panel" style={{ marginTop: "15px" }}>
              <div className="x_content">

                <MUIDataTableComponent datatableClassName={'top5ODTable'} data={this.state.agentData} columns={this.state.agentColumn} options={agentOptions} title={''} />

              </div>
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            <div className="x_panel">
              <div className="x_content">

                <MUIDataTableComponent datatableClassName={'top5Competitor'} data={this.state.topMarketData} columns={this.state.topMarketColumn} options={topMarketOptions} title={this.state.tableTitle1} />

              </div>
            </div>

            {/* <div role="tabpanel" className="tab-pane fade" id="Section3">

                      <MUIDataTableComponent datatableClassName={'agencyTable'} data={this.state.posRegionDatas} columns={this.state.regioncolumn} options={odAndAgencyFlowOptions} title={this.state.tableTitle} />

                    </div> */}

          </div>
        </div>

        <div>
          <ChartModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>

        <div>
          <DatatableModelDetails datas={this.state.modelRegionDatas} columns={this.state.modelregioncolumn} />
        </div>


      </div >

    );
  }
}

export default AgentAnalysis;