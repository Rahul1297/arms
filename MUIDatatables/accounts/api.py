from rest_framework import generics, permissions
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import UserSerializer,LoginSerializer, RegisterSerializer 
from knox.views import LoginView as KnoxLoginView
from knox.auth import TokenAuthentication
from rest_framework.permissions import IsAdminUser
# from django.views.decorators.csrf import csrf_exempt


class RegisterAPI(generics.GenericAPIView):

    permission_classes = (IsAdminUser,)
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        User = serializer.save()

        return Response({
            "User": UserSerializer(User, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(User)[1]
        })

class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = ( permissions.IsAuthenticated,)

    # permission_classes = [ permissions.IsAuthenticated,]
    # @csrf_exempt
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        

        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })

class UserAPI(generics.RetrieveAPIView):
      
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


